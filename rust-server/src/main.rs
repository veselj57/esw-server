mod messages;
use tokio::net::{TcpListener};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use protobuf::Message;
use crate::messages::{Request};
use crate::messages::Response;
use crate::messages::Response_Status;
use crate::messages::Request_oneof_msg;
use std::io::prelude::*;
use flate2::read::GzDecoder;
use std::sync::{Arc};
use dashmap::DashSet;
use tokio::runtime::Builder;
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;
type Words = DashSet<u64>;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let listener = TcpListener::bind("[::]:8888").await?;

    let words: Arc<Words> = Arc::new(DashSet::with_capacity(100000));

    let runtime = Builder::new_multi_thread()
        .worker_threads(32)
        .build()
        .unwrap();

    //Accept clients
    loop {
        let (mut socket, _) = listener.accept().await?;

        let set = words.clone();

        //For each new client spawn task in Tokio environment
        runtime.spawn(async move {

            // Request - response loop
            loop {
                //Read message size
                let size_bytes = socket.read_u32().await;

                // if there is no message close client
                if size_bytes.is_err() { break }

                //Read a message into buffer
                let mut buf = vec![0u8; size_bytes.unwrap()  as usize];
                socket.read_exact(&mut buf).await.expect("Need {size} bytes to decompress the message");

                //Parse the proto message
                let mut request:Request = Request::parse_from_bytes(&buf).unwrap();

                // Determine a type of the message
                if let ::std::option::Option::Some(ref v) = request.msg {
                    let response = match v {
                        &Request_oneof_msg::getCount(ref v) =>get_count(&*set) ,
                        &Request_oneof_msg::postWords(ref v) =>post_words(request.take_postWords().data, &set)
                    };

                    //Respond back
                    let data = response.write_to_bytes().unwrap();
                    socket.write_i32(data.len() as i32).await;
                    socket.write_all(&data).await;
                }else {
                    panic!("Strange looking message was spotted near by.")
                };
            }
        });
    }
}

/**
* Decode words and save their has into  HashSet
*/
fn post_words(data: Vec<u8>, db: &Words) -> Response {
    let mut decoder = GzDecoder::new(data.as_slice());
    let mut input = String::new();
    decoder.read_to_string(&mut input).unwrap();

    for word in  input.split_whitespace() {
        let mut s = DefaultHasher::new();
        word.hash(&mut s);
        db.insert( s.finish());
    }

    let mut response = Response::new();
    response.status = Response_Status::OK;
    return response
}

/**
* Clear the words DashSet and respond
*/
fn get_count(words: &Words) -> Response {
    let mut response = Response::new();
    response.status = Response_Status::OK;
    response.counter = words.len() as i32;
    words.clear();
    return response
}