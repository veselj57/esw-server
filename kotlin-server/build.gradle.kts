import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.32"
    id("com.google.protobuf") version "0.8.15"
    id("com.github.johnrengelman.shadow") version "6.1.0"
    application
}

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    val ktor = "1.5.3"

    implementation ("io.ktor:ktor-network:$ktor")
    implementation("com.google.protobuf:protobuf-java:3.15.8")
    implementation("io.ktor:ktor-server-netty:$ktor")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")
    implementation(kotlin("stdlib-jdk8"))

}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClassName = "ServerKt"
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}


sourceSets {
    main {
        proto {
            // In addition to the default 'src/main/proto'
            srcDir("src/main/protobuf")
            srcDir("src/main/protocolbuffers")
            // In addition to the default "**/*.proto" (use with caution).
            // Using an extension other than "proto" is NOT recommended,
            // because when proto files are published along with class files, we can
            // only tell the type of a file from its extension.
            include("**/*.protodevel")
        }
        java{
            srcDirs("build/generated/source/proto/main/java")

        }
    }
}

tasks {
    named<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar>("shadowJar") {
        archiveBaseName.set("shadow")
        mergeServiceFiles()
        manifest {
            attributes(mapOf("Main-Class" to "serverKt"))
        }

        minimize()
    }
}

tasks {
    build {
        dependsOn(shadowJar)
    }
}