import cz.cvut.fel.esw.server.proto.Request
import cz.cvut.fel.esw.server.proto.Response
import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import io.ktor.utils.io.*
import io.ktor.utils.io.core.*
import kotlinx.coroutines.*
import java.io.ByteArrayInputStream
import java.net.InetSocketAddress
import java.nio.charset.Charset
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.Executors
import java.util.zip.GZIPInputStream


//Word Bank
val words = ConcurrentSkipListSet<String>()

fun main() {
    val selector = Executors.newCachedThreadPool().asCoroutineDispatcher()
    val workers = Executors.newCachedThreadPool().asCoroutineDispatcher()

    runBlocking {
        //Server Socket
        val server = aSocket(ActorSelectorManager(selector)).tcp().bind(InetSocketAddress(8888))


        //Accept clients in loop
        while (true) {
            val socket = server.accept()

            //For each client launch new Coroutine i.e. lightweight task/thread on workers pool
            launch(workers) {
                request(socket)
            }
        }
    }
}

suspend fun request(socket: Socket) = coroutineScope {
    val input = socket.openReadChannel()
    val output = socket.openWriteChannel()

    // Keep list of concurrent posts for this request
    val jobs = mutableListOf<Job>()

    try {
        while (true){
            input.awaitContent()

            val size = input.readInt(ByteOrder.BIG_ENDIAN)
            val request = Request.parseFrom(input.readPacket(size).readBytes())

            //Determine type of request
            when(request.msgCase!!){
                Request.MsgCase.GETCOUNT -> {
                    jobs.joinAll()
                    respond(output, getCount(request))
                }
                Request.MsgCase.POSTWORDS -> {
                    jobs.add(launch {
                        respond(output, postWords(request))
                    })
                }
                Request.MsgCase.MSG_NOT_SET -> throw Exception("Illegal argument")
            }
        }
    }catch (e: Exception){ // Look for EOF
        socket.close()
    }
}

/**
 * Process post request and return response
 */
inline fun postWords(request: Request): Response{
    val data = request.postWords.data.toByteArray()

    processWords(data)

    val builder = Response.newBuilder()
    builder.status = Response.Status.OK

    return builder.build()
}

/**
 * Reset the word counter and create response
 */
inline fun getCount(request: Request): Response {
    val builder = Response.newBuilder()
    builder.status = Response.Status.OK
    builder.counter = words.size

    words.clear()

    return builder.build()
}

/**
 * Decompress byte array (gzip) and add words one by one into word bank
 */
inline fun processWords(data: ByteArray){
    val to =  GZIPInputStream(ByteArrayInputStream(data))
        .readBytes()
        .toString(Charset.forName("UTF-8"))
        .split(" ", "\n", "\r", "\t")

    words.addAll(to)
    words.remove("")
}

/**
 * Serialize given message and respond to socket
 */
suspend inline fun respond(output: ByteWriteChannel, response: Response){
    val responseBytes = response.toByteArray()

    output.writeInt(responseBytes.size, ByteOrder.BIG_ENDIAN)
    output.writeFully(responseBytes)
    output.flush()
}

